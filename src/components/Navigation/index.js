import React from 'react';
import TabButton from './TabButton';

class Navigation extends React.Component {
    constructor() {
        super();
        this.state = {
            templatesActive: true,
            productsActive: false,
        }
    }

    sendNavProps = () => {
        const { templatesActive, productsActive } = this.state;

        this.props.callbackNav(templatesActive, productsActive);
    }

    activeTab = () => {
        this.setState({
            templatesActive: !this.state.templatesActive,
            productsActive: !this.state.productsActive,
        })
        this.sendNavProps();
    }

    render() {
        return(
            <div className="container">
                <div className="row">
                    <div className="nav-container">
                        <TabButton
                            title="Templates" 
                            activeTab={this.state.templatesActive} 
                            activateTab={this.activeTab}
                        />
                        <TabButton
                            title="Products" 
                            activeTab={this.state.productsActive}
                            activateTab={this.activeTab} 
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default Navigation;