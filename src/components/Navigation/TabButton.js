import React from 'react';

const TabButton = (props) => (
    <button
        onClick={props.activateTab} 
        className={props.activeTab ? "tab-button active-button"  : "tab-button"} 
    >
        {props.title}
    </button>
)

export default TabButton;