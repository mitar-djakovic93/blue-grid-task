import React from 'react';

const Title = () => (
    <div className="container">
        <div className="row">
            <div className="title">
                <h1>Manage Subscription</h1>
            </div>
        </div>
    </div>
)

export default Title;