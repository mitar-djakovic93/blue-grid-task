import React from 'react';

class NextButton extends React.Component {

    sendProps = () => {
        
    }
    render() {
        return(
            <div className='next-button'>
                <button type="button" className={this.props.templateIdActive ? 'next' : 'disabled'}>Next</button>
                <p>or</p>
                <button type="button" className='back'>Back</button>
            </div>
        )
    }
}

export default NextButton;