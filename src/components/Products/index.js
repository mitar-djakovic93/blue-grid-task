import React from 'react';

const Product = (props) => {
    const handleClick = () => {
        props.getTemplateIdCallback(props.id)
    }
    const selected = props.selected;
    return(
        <div className='product'>
            <div className='product-title'>
                {
                    props.templatesActive ? <h1>Edge Delivery | {props.name}</h1> : <h1>{props.name}</h1>
                }
            </div>
            <div className='product-container'>
                <h3 className='product-description'>{props.description}</h3>
                <ul>
                    {
                        props.features.map((feature, index) => (
                            <li key={index} >{feature}</li>
                        ))
                    }
                </ul>
                <h3>{props.price}</h3>
            </div>
            <div className='button-container'>
                <button 
                    className={selected ? 'selected' : ''} 
                    onClick={() => handleClick()}
                >
                    Select
                </button>
            </div>
        </div>
    )
}

export default Product;