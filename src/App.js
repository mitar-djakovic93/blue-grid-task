import React from 'react';
import axios from 'axios';
import Title from './components/Title';
import Navigation from './components/Navigation';
import Product from './components/Products';
import NextButton from './components/NextButton';
import './scss/style.scss';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            templatesActive: true,
            productsActive: false,
            templateId: '',
            templatesList: [],
            productsList: [],
            selectetTemplate: [],
            selectedProducts: [],
            templateIdActive: false,
            selectedTemplate: ''
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    getNavPropsCallback = (templatesActive, productsActive) => {
        this.setState({
            templatesActive: !templatesActive,
            productsActive: !productsActive
        })
    }

    getTemplateIdCallback = (id) => {
        const selectedTemplate = this.state.templatesList.filter((temp) => temp._id === id)
        console.log(selectedTemplate)
        this.setState({
            templateId: id,
            templateIdActive: true,
            selectedTemplate
        })
    }

    getProductIdCallback = (id) => {
        console.log(id);
        // console.log(selected);
        // const list = this.state.selectedProducts;
        // list.push(id);
        // this.setState({
        //     selectedProducts: list
        // })
    }

    fetchData = () => {
        axios.get('https://api.kadporastembicu.dev/v1/products').then((res) => {
            this.setState({
                productsList: res.data
            })
        })
        .catch((err) => {
            console.log(err);
        });
        axios.get('https://api.kadporastembicu.dev/v1/templates').then((res) => {
            this.setState({
                templatesList: res.data
            })
        })
        .catch((err) => {
            console.log(err);
        });
    }

    render() {
        return (
            <div className="App">
                <Title />
                <Navigation callbackNav={this.getNavPropsCallback} />
                <div className="container">
                    <div className="row">
                        {
                            this.state.templatesActive ?
                                this.state.templatesList.map((template) => (
                                    <div 
                                        className='col-md-4'
                                        key={template._id}
                                    >
                                        <Product
                                            templatesActive={this.state.templatesActive}
                                            id={template._id}
                                            selectTemplate={this.selectTemplate}
                                            name={template.name}
                                            description={template.description}
                                            price={template.price}
                                            features={template.features}
                                            getTemplateIdCallback={this.getTemplateIdCallback}
                                            selected={this.state.templateId === template._id ? true : false}
                                        />
                                    </div>
                                ))
                            :
                            this.state.productsList.map((product) => (
                                <div 
                                    className='col-md-3'
                                    key={product._id}
                                >
                                    <Product
                                        id={product._id}
                                        name={product.name}
                                        description={product.description}
                                        price={product.price}
                                        features={product.features}  
                                        getTemplateIdCallback={this.getProductIdCallback}
                                        selected={this.state.selected}
                                    />
                                </div>
                            ))
                        }
                    <NextButton 
                        templateIdActive={this.state.templateIdActive}
                        selectedTemplate={this.state.selectedTemplate} 
                    />
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
